package br.com.threads.entity;

public class Product {

	private String name;
	private int serial;

	public Product(String name, int number) {
		this.name = name;
		this.serial = number;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getNumber() {
		return serial;
	}

	public void setNumber(int number) {
		this.serial = number;
	}

	@Override
	public String toString() {
		return "Nome: " + name + ", serial: " + serial;
	}
}