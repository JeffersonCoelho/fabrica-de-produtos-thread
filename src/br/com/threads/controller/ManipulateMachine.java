package br.com.threads.controller;

import br.com.threads.entity.Product;

public class ManipulateMachine extends Thread implements Runnable {

	private final static int MAX_PROCESS = 50;
	private final static int FINISH_MACHINE = 100;

	private Product product;
	private int qntProcess = 0;
	private int advancementProcess = 0; // Avanço do produto no processo
	private int progressProcess = 0; // Onde o produto está no processo

	public ManipulateMachine(Product product) {
		this.product = product;
	}

	@Override
	public void run() {

		while (progressProcess < FINISH_MACHINE) {

			progressProduct();
			printPosition();
			swapProcess();
		}

		finishProduct();
	}

	private void progressProduct() {
		qntProcess++;

		advancementProcess = (int) (Math.random() * MAX_PROCESS);
		progressProcess += advancementProcess;

		if (progressProcess > FINISH_MACHINE) {
			progressProcess = FINISH_MACHINE;
		}
	}

	private void finishProduct() {
		System.out.println(product.getName() + " já está pronto! Foram necessários " + qntProcess + " processos!");
	}

	private void printPosition() {
		System.out.println(product.getName() + " avançou " + advancementProcess + "% no processo e já passou "
				+ progressProcess + "%.");
	}

	private void swapProcess() {
		yield();
	}
}