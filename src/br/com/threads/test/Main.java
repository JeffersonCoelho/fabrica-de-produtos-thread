package br.com.threads.test;

import br.com.threads.controller.ManipulateMachine;
import br.com.threads.entity.Product;

public class Main {

	final static int NUM_PRODUCTS = 5;

	public static void main(String[] args) {

		for (int i = 1; i <= NUM_PRODUCTS; i++) {
			new ManipulateMachine(new Product(("Produto " + i), i)).start();
		}

	}
}